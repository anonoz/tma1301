function y = ACG(n, min, max) %Additive Congruential Generator
    
    
    c = 53;
    x = rand()*max;
    x = ceil(x);
    
    for i=1:n
        
        z = x + c;
        y(i) = (ceil(mod(z, max)));
        
        if y(i) < max-min;
            y(i) = y(i) + min;
        end
        
        x = y(i);
    end