Love Lane Simulator
===================

![Love Lane Photo](http://1.bp.blogspot.com/-TA3WdK08Fjc/UDX500eC8fI/AAAAAAAAVm8/t-t_z0J3dwc/s640/12.jpeg)

Instruction
-----------

To run the sim, navigate into the project folder. 

Inside FreeMat console, type `lovelane_simulate(num_of_servers, num_of_customers, random_generator)`, for example:

```
#!matlab

lovelane_simulate(2, 50, 'LCG')
```

Random Generators Available
---------------------------

Generator | Description
---------:|:-----------
LCG       | Linear Congruential Generator
REX       | Random Exponential Distribution
ACG       | Additive Congruential Generator
MCG       | Multiplicative Congruential Generator
RUN       | Random Uniform Generator

If left blank it will default to **RUN**.