function y = MCG(n, min, max) %Multiplicative Congruential Generator
    
    
    a = 13;
    x = rand()*max;
    x = ceil(x);
    
    for i=1:n
        
        z = a*x;
        y(i) = (ceil(mod(z, max)));
        
        if y(i) < max-min;
            y(i) = y(i) + min;
        end
        
        x = y(i);
    end